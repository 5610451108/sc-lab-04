package Viewer;


import java.awt.FlowLayout;
import java.awt.print.Pageable;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Gui {

	public static <E> void main(String[] args) {
		JFrame frame = new JFrame();
        frame.setSize(500,300);
        frame.setTitle("Lab 4");
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLayout(new FlowLayout());
        
    	JPanel panelLeft;
    	JPanel panelRight;
    
    	panelLeft = new JPanel();
    	panelRight = new JPanel();
    	
    	JTextArea textArea;
    	textArea= new JTextArea("out put", 5,20);
    	
//    	JComboBox<E> comboBox;
//    	comboBox = new JComboBox<E>();
////		comboBox.addItem(1);
//		
//    	panelLeft.add(comboBox);
//    	
    	panelRight.add(textArea);
    	
    	frame.add(panelLeft);
    	frame.add(panelRight);
	}
}
